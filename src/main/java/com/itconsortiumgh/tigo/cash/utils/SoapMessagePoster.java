package com.itconsortiumgh.tigo.cash.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SoapMessagePoster {
	public String sendMessage(String endpointURL, String soapMessage){
		String result = "";
		BufferedReader in = null;
		OutputStream out = null;
		
		// Create the connection where we're going to send the file.
		URL url =null;
		try {
			url = new URL(endpointURL);

			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) connection;

			// Open the input file. After we copy it to a byte array, we can see
			// how big it is so that we can set the HTTP Cotent-Length
			// property. (See complete e-mail below for more on this.)

			byte[] b = soapMessage.getBytes();
			// Set the appropriate HTTP parameters.
			httpConn.setRequestProperty( "Content-Length",
					String.valueOf( b.length ) );
			httpConn.setRequestProperty("Content-Type","text/xml; charset=utf-8");
			httpConn.setRequestProperty("SOAPAction","");
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);

			// Everything's set up; send the XML that was read in to b.
			out = httpConn.getOutputStream();
			out.write(b);    
			out.flush();

			// Read the response and write it to standard out.

			InputStream _is = null;
			if (httpConn.getResponseCode() >= 400) {
				_is = httpConn.getErrorStream(); 
			} else { 
				/* error from server */ 
				_is = httpConn.getInputStream(); 
			} 
			InputStreamReader isr = new InputStreamReader(_is);
			in = new BufferedReader(isr);
			String inputLine;

			while ((inputLine = in.readLine()) != null)
				result = result.concat(inputLine);
			//            System.out.println(inputLine);
			//        return result;
		}catch(MalformedURLException e){
			log.error("malformedURL exception");
			result = e.getMessage();
		} catch (IOException e) {
			log.error("IOException");
			result = e.getMessage();
		}finally{
			try {
				if(out!=null){
					out.close();
				}
				if(in!=null){
					in.close();
				}

			} catch (IOException e) {
				log.error("IOException inside finally clause");
				result = e.getMessage();
			}
		}
		return result;
	}
}

