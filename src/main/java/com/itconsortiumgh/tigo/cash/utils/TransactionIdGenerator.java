package com.itconsortiumgh.tigo.cash.utils;

import java.util.concurrent.atomic.AtomicReference;

public class TransactionIdGenerator {
	private static AtomicReference<Long> currentTime = 
			new AtomicReference<>(System.currentTimeMillis());

	public static Long nextId() {
		Long prev; 
		Long next = System.currentTimeMillis();
		do {
			prev = currentTime.get();
			next = next > prev ? next : prev + 1;
		} while (!currentTime.compareAndSet(prev, next));
		return next;
	}
}
