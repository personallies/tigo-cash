package com.itconsortiumgh.tigo.cash.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties
public class ApplicationProperties {
	private String iniPayUrl;
	private String debugFlag;
	private String username;
	private String password;
	private String consumerID;
	private String initMsisdn;
	private String webUser;
	private String webPassword;
	private String countryCode;
	private String merchantName;
	private String merchantCode;
	private String externalCategory;
	private String externalChannel;
	private String minutesToExpire;
	private String notificationChannel;
	private String purchaseCode;
	private String ussdPurchaseCode;
	private String cancelTransactionCode;
	private String pin;
	private String reference;
	private String payBillUrl;
}
