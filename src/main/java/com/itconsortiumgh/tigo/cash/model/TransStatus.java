package com.itconsortiumgh.tigo.cash.model;

import lombok.Data;

@Data
public class TransStatus {
	public static final String AUTHORIZED = "Authorized";
	public static final String PENDING = "Pending";
	public static final String SUCCESS = "Success";
	public static final String FAIL = "Fail";
}
