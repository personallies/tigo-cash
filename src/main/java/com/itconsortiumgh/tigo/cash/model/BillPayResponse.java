package com.itconsortiumgh.tigo.cash.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@XmlType(propOrder={"id","status","code","description"})
@XmlRootElement(name="BILLPAYRESPONSE")
public class BillPayResponse {
//	private String transactionID;
//	private String correlationID;
	private String id;
	private String status;
	private String code;
	private String description;

}
