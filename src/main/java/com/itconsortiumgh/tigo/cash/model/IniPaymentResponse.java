package com.itconsortiumgh.tigo.cash.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class IniPaymentResponse {
	private String transactionID;
	private String correlationID;
	private String status;
	private String code;
	private String description;
	private String paymentId;
	private String paymentReference;

}
