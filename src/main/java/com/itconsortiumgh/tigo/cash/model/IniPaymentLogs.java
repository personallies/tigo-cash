package com.itconsortiumgh.tigo.cash.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table
public class IniPaymentLogs {
	@Id
	@Column(length=50)
	private String transactionID;
	@Column(length=50)
	private String correlationID;
	@Column(length=50)
	private String status;
	@Column(length=50)
	private String code;
	@Column(length=200)
	private String description;
	@Column(length=50)
	private Date created;
	@Column(length=50)
	private Date processed;
}
