package com.itconsortiumgh.tigo.cash.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class ResponseCode {
	public static final String SUCCESS = "00";
	public static final String FAIL = "15";
	
}
