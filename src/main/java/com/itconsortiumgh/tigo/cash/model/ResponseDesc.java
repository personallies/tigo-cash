package com.itconsortiumgh.tigo.cash.model;

public class ResponseDesc {
	public static final String SUCCESS = "Callback successful";
	public static final String FAIL_SEARCH = "Callback failed - Could not find Transaction log in database";
	public static final String FAIL_INSERT = "Callback failed - Could not save Transaction log into database";
}
