package com.itconsortiumgh.tigo.cash.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Response {
	private String responseCode;
	private String responseMessage;

}
