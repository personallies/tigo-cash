package com.itconsortiumgh.tigo.cash.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class PayBillRequest {
	private String msisdn;
	private String amount;
	
}
