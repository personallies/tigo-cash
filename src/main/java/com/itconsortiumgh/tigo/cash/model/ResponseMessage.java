package com.itconsortiumgh.tigo.cash.model;


public class ResponseMessage {
	public static final String SUCCESS_CALLBACK = "Callback Successful";
	public static final String FAIL_CALLBACK = "Callback Failed";
	public static final String TRANS_SUCCESS = "Transaction Successful";
	public static final String TRANS_EXPIRED = "Transaction Expired";
	public static final String TRANS_CANCELLED= "Transaction Cancelled";
	public static final String TRANS_FAILED= "Transaction Failed";
	public static final String SUCCESS = "SUCCESS";
	public static final String FAIL = "FAILED";
	
	

}
