package com.itconsortiumgh.tigo.cash.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class IniPaymentRequest {
	private String custMsisdn;
	private String itemName;
	private String amount;

}
