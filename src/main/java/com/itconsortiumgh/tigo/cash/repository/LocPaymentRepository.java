package com.itconsortiumgh.tigo.cash.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.tigo.cash.model.IniPaymentLogs;

@Repository
public interface LocPaymentRepository extends CrudRepository<IniPaymentLogs, Long>{
	public IniPaymentLogs findByTransactionID(String transactionID);
}
