package com.itconsortiumgh.tigo.cash.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.itconsortiumgh.tigo.cash.model.BillPayResponse;
import com.itconsortiumgh.tigo.cash.model.IniPaymentLogs;
import com.itconsortiumgh.tigo.cash.model.IniPaymentRequest;
import com.itconsortiumgh.tigo.cash.model.IniPaymentResponse;
import com.itconsortiumgh.tigo.cash.model.PayBillRequest;
import com.itconsortiumgh.tigo.cash.model.ResponseCode;
import com.itconsortiumgh.tigo.cash.model.ResponseDesc;
import com.itconsortiumgh.tigo.cash.model.ResponseMessage;
import com.itconsortiumgh.tigo.cash.properties.ApplicationProperties;
import com.itconsortiumgh.tigo.cash.repository.LocPaymentRepository;
import com.itconsortiumgh.tigo.cash.service.BusinessLogic;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class Controller {
	@Autowired
	BusinessLogic businessLogic;
	@Autowired
	LocPaymentRepository locPaymentRepository;
	@Autowired
	ApplicationProperties applicationProperties;

	@RequestMapping(value = "initiate/payment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public IniPaymentResponse initiatePayment(@RequestBody IniPaymentRequest iniPaymentRequest) {
		log.info("The Json request : {}",iniPaymentRequest);
		return businessLogic.initiatePayment(iniPaymentRequest);
	}

	@PostMapping("/pay/bill")
	public String payBill(@RequestBody PayBillRequest payBillRequest){
		log.info("The Json request : {}", payBillRequest);
		return businessLogic.payBill(payBillRequest);
	}
	
	@RequestMapping(value = "callback/local/payment", method = RequestMethod.GET, produces = "application/xml")
	@ResponseBody
	public BillPayResponse callbackPayment(@RequestParam(value = "transactionID") String transactionID,
			@RequestParam(value = "correlationID") String correlationID, @RequestParam(value = "status") String status,
			@RequestParam(value = "code") String code, @RequestParam(value = "description") String description) {
		BillPayResponse billPayResponse = new BillPayResponse();
		billPayResponse.setId(transactionID);
		
		log.info("the request body coming:{}", billPayResponse);

		IniPaymentLogs locPaymentLogs = new IniPaymentLogs();
		locPaymentLogs.setTransactionID(transactionID);
		locPaymentLogs.setCorrelationID(correlationID);
		locPaymentLogs.setCode(code);
		locPaymentLogs.setDescription(description);
		locPaymentLogs.setProcessed(new Date());
		
		if (applicationProperties.getPurchaseCode().equalsIgnoreCase(code)) {
			locPaymentLogs.setStatus(ResponseMessage.TRANS_SUCCESS);
		} else if (applicationProperties.getUssdPurchaseCode().equalsIgnoreCase(code)) {
			locPaymentLogs.setStatus(ResponseMessage.TRANS_EXPIRED);
		} else if (applicationProperties.getCancelTransactionCode().equalsIgnoreCase(code)) {
			locPaymentLogs.setStatus(ResponseMessage.TRANS_CANCELLED);
		} else {
			locPaymentLogs.setStatus(ResponseMessage.TRANS_FAILED);
		}
		
		IniPaymentLogs foundLocPaymentLogs = locPaymentRepository.findByTransactionID(transactionID);
		if(foundLocPaymentLogs != null){
			IniPaymentLogs savedLocPaymentLogs = locPaymentRepository.save(foundLocPaymentLogs);
			if(savedLocPaymentLogs != null){
				billPayResponse.setStatus(ResponseMessage.SUCCESS);
				billPayResponse.setCode(ResponseCode.SUCCESS);
				billPayResponse.setDescription(ResponseDesc.SUCCESS);
			}else{
				billPayResponse.setStatus(ResponseMessage.FAIL);
				billPayResponse.setCode(ResponseCode.FAIL);
				billPayResponse.setDescription(ResponseDesc.FAIL_INSERT);
			}
		}else{
			billPayResponse.setStatus(ResponseMessage.FAIL);
			billPayResponse.setCode(ResponseCode.FAIL);
			billPayResponse.setDescription(ResponseDesc.FAIL_SEARCH);
		}
		return billPayResponse;
	}


}
