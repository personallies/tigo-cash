package com.itconsortiumgh.tigo.cash.service;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.stereotype.Service;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.itconsortiumgh.tigo.cash.properties.ApplicationProperties;

@Service
public class PayBillRequestBuilder {

	
	public static void main(String[] args) {
		
		PayBillRequestBuilder payBillRequestBuilder = new PayBillRequestBuilder();
		ApplicationProperties applicationProperties = new ApplicationProperties();
		payBillRequestBuilder.getXmlRequest(applicationProperties,"kel", "kel", "kel", "kel");
	}
	
	public String getXmlRequest(ApplicationProperties applicationProperties, String vTransactionId, String vCorrelationId, String vMsisdn, String vAmount){
		
		String requestXml = "";
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation impl = builder.getDOMImplementation();

			Document message = impl.createDocument(null, null, null);
			
			Element envelope = message.createElement("soapenv:Envelope");
			envelope.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
			envelope.setAttribute("xmlns:v1", "http://xmlns.tigo.com/MFS/PaymentsRequest/V1");
			envelope.setAttribute("xmlns:v3", "http://xmlns.tigo.com/RequestHeader/V3");
			envelope.setAttribute("xmlns:v2", "http://xmlns.tigo.com/ParameterType/V2");
			envelope.setAttribute("xmlns:cor", "http://soa.mic.co.af/coredata_1");
			message.appendChild(envelope);
			
			Element header = message.createElement("soapenv:Header");
			header.setAttribute("xmlns:wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wsswssecurity-secext-1.0.xsd");
			envelope.appendChild(header);
			
			Element debugFlag = message.createElement("cor:debugFlag");
			debugFlag.setTextContent(applicationProperties.getDebugFlag());
			header.appendChild(debugFlag);
			
			Element security = message.createElement("wsse:Security");
			header.appendChild(security);
			
			Element usernameToken = message.createElement("wsse:UsernameToken");
			security.appendChild(usernameToken);
			
			Element username = message.createElement("wsse:Username");
			username.setTextContent(applicationProperties.getUsername());
			usernameToken.appendChild(username);
			
			Element password = message.createElement("wsse:Password");
			password.setTextContent(applicationProperties.getPassword());
			usernameToken.appendChild(password);
			
			Element body = message.createElement("soapenv:Body");
			envelope.appendChild(body);
			
			Element payBillRequest = message.createElement("v1:payBillRequest");
			body.appendChild(payBillRequest);
			
			Element requestHeader = message.createElement("v3:RequestHeader");
			payBillRequest.appendChild(requestHeader);
			
			Element generalConsumerInfo = message.createElement("v3:GeneralConsumerInformation");
			requestHeader.appendChild(generalConsumerInfo);
			
			Element consumerId = message.createElement("v3:consumerID");
			consumerId.setTextContent(applicationProperties.getConsumerID());
			generalConsumerInfo.appendChild(consumerId);
			
			Element transactionId = message.createElement("v3:transactionID");
			transactionId.setTextContent(vTransactionId);
			generalConsumerInfo.appendChild(transactionId);
			
			Element country = message.createElement("v3:country");
			country.setTextContent(applicationProperties.getCountryCode());
			generalConsumerInfo.appendChild(country);
			
			Element correlationId = message.createElement("v3:correlationID");
			correlationId.setTextContent(vCorrelationId);
			generalConsumerInfo.appendChild(correlationId);
			
			Element requestBody = message.createElement("v1:requestBody");
			payBillRequest.appendChild(requestBody);
			
			Element reference = message.createElement("v1:reference");
			reference.setTextContent(applicationProperties.getReference());
			requestBody.appendChild(reference);
			
			Element sourceWallet = message.createElement("v1:sourceWallet");
			requestBody.appendChild(sourceWallet);
			
			Element sourceMsisdn = message.createElement("v1:msisdn");
			sourceMsisdn.setTextContent(applicationProperties.getInitMsisdn());
			sourceWallet.appendChild(sourceMsisdn);
			
			Element targetWallet = message.createElement("v1:targetWallet");
			requestBody.appendChild(targetWallet);
			
			Element targetMsisdn = message.createElement("v1:msisdn");
			targetMsisdn.setTextContent(vMsisdn);
			targetWallet.appendChild(targetMsisdn);
			
			Element pin = message.createElement("v1:password");
			pin.setTextContent(applicationProperties.getPin());
			requestBody.appendChild(pin);
			
			Element amount = message.createElement("v1:amount");
			amount.setTextContent(vAmount);
			requestBody.appendChild(amount);
			
			DOMSource domSource = new DOMSource(message);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			// transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
			// "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

			java.io.StringWriter sw = new java.io.StringWriter();
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
			requestXml = sw.toString();

			System.out.println(requestXml);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return requestXml;
	}
}
