package com.itconsortiumgh.tigo.cash.service;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.itconsortiumgh.tigo.cash.model.IniPaymentResponse;

@Service
public class IniPayResponseBuilder {

	public IniPaymentResponse produceResponse(String responseXml) {
		IniPaymentResponse iniPaymentResponse = new IniPaymentResponse();

		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(responseXml)));
			document.getDocumentElement().normalize();

			NodeList responses = document.getElementsByTagName("");

			for (int i = 0; i < responses.getLength(); i++) {
				Node firstServiceNode = responses.item(i);

				if (firstServiceNode.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) firstServiceNode;

					NodeList correlationIDList = element.getElementsByTagName("v3:correlationID");
					Element correlationIDElement = (Element) correlationIDList.item(0);
					String correlationID = correlationIDElement.getTextContent();

					NodeList statusList = element.getElementsByTagName("v3:status");
					Element statusElement = (Element) statusList.item(1);
					String status = statusElement.getTextContent();
					
					NodeList codeList = element.getElementsByTagName("v3:code");
					Element codeElement = (Element) codeList.item(2);
					String code = codeElement.getTextContent();
					
					NodeList descriptionList = element.getElementsByTagName("v3:description");
					Element descriptionElement = (Element) descriptionList.item(3);
					String description = descriptionElement.getTextContent();

					NodeList paymentIdList = element.getElementsByTagName("v1:paymentId");
					Element paymentIdElement = (Element) paymentIdList.item(2);
					String paymentId = paymentIdElement.getTextContent();
					
					NodeList paymentReferenceList = element.getElementsByTagName("v1:paymentReference");
					Element paymentReferenceElement = (Element) paymentReferenceList.item(2);
					String paymentReference = paymentReferenceElement.getTextContent();
					
					iniPaymentResponse.setCorrelationID(correlationID);
					iniPaymentResponse.setStatus(status);
					iniPaymentResponse.setCode(code);
					iniPaymentResponse.setDescription(description);
					iniPaymentResponse.setPaymentId(paymentId);
					iniPaymentResponse.setPaymentReference(paymentReference);
				}
				
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return iniPaymentResponse;
	}

}
