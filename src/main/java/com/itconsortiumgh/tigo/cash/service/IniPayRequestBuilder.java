package com.itconsortiumgh.tigo.cash.service;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.stereotype.Service;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.itconsortiumgh.tigo.cash.properties.ApplicationProperties;

@Service 
public class IniPayRequestBuilder {
	
	public static void main(String[] args) {
		IniPayRequestBuilder locCallRequestBuilder = new IniPayRequestBuilder();
		ApplicationProperties applicationProperties = new ApplicationProperties();
		locCallRequestBuilder.getXmlRequest(applicationProperties,"kel","kel","kel", "kel", "kel", "kel");
	}

	public String getXmlRequest(ApplicationProperties applicationProperties, String vTransactionID, String vCorrelationID, String vMsisdn, String vPaymentReference, String vItemName, String vAmount){
		String requestXml = "";
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation impl = builder.getDOMImplementation();

			Document message = impl.createDocument(null, null, null);
			
			Element envelope = message.createElement("soapenv:Envelope");
			envelope.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
			message.appendChild(envelope);
			
			Element header = message.createElement("soapenv:Header");
			header.setAttribute("xmlns:wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
			envelope.appendChild(header);
			
			Element debugFlag = message.createElement("cor:debugFlag");
			debugFlag.setAttribute("xmlns:cor", "http://soa.mic.co.af/coredata_1");
			debugFlag.setTextContent(applicationProperties.getDebugFlag());
			header.appendChild(debugFlag);
			
			Element security = message.createElement("wsse:Security");
			header.appendChild(security);
			
			Element usernameToken = message.createElement("wsse:UsernameToken");
			security.appendChild(usernameToken);
			
			Element username = message.createElement("wsse:Username");
			username.setTextContent(applicationProperties.getUsername());
			usernameToken.appendChild(username);
			
			Element password = message.createElement("wsse:Password");
			password.setTextContent(applicationProperties.getPassword());
			usernameToken.appendChild(password);
			
			Element body = message.createElement("soapenv:Body");
			envelope.appendChild(body);
			
			Element purchaseInitiateRequest = message.createElement("v1:PurchaseInitiateRequest");
			purchaseInitiateRequest.setAttribute("xmlns:v3", "http://xmlns.tigo.com/RequestHeader/V3");
			purchaseInitiateRequest.setAttribute("xmlns:v1", "http://xmlns.tigo.com/MFS/PurchaseInitiateRequest/V1");
			purchaseInitiateRequest.setAttribute("xmlns:v2", "http://xmlns.tigo.com/ParameterType/V2");
			body.appendChild(purchaseInitiateRequest);
			
			Element requestHeader = message.createElement("v3:RequestHeader");
			purchaseInitiateRequest.appendChild(requestHeader);
			
			Element generalConsumerInformation = message.createElement("v3:GeneralConsumerInformation");
			requestHeader.appendChild(generalConsumerInformation);
			
			Element consumerID = message.createElement("v3:consumerID");
			consumerID.setTextContent(applicationProperties.getConsumerID());
			generalConsumerInformation.appendChild(consumerID);
			
			Element transactionID = message.createElement("v3:transactionID");
			transactionID.setTextContent(vTransactionID);
			generalConsumerInformation.appendChild(transactionID);
			
			Element country = message.createElement("v3:country");
			country.setTextContent(applicationProperties.getCountryCode());
			generalConsumerInformation.appendChild(country);
			
			Element correlationID = message.createElement("v3:correlationID");
			correlationID.setTextContent(vCorrelationID);
			generalConsumerInformation.appendChild(correlationID);
			
			Element requestBody = message.createElement("v1:requestBody");
			purchaseInitiateRequest.appendChild(requestBody);
			
			Element customerAccount = message.createElement("v1:customerAccount");
			requestBody.appendChild(customerAccount);
			
			Element msisdn = message.createElement("v1:msisdn");
			msisdn.setTextContent(vMsisdn);
			customerAccount.appendChild(msisdn);
			
			Element initiatorAccount = message.createElement("v1:initiatorAccount");
			requestBody.appendChild(initiatorAccount);
			
			Element initMsisdn = message.createElement("v1:msisdn");
			initMsisdn.setTextContent(applicationProperties.getInitMsisdn());
			initiatorAccount.appendChild(initMsisdn);
			
			Element paymentReference = message.createElement("v1:paymentReference");
			paymentReference.setTextContent(vPaymentReference);
			requestBody.appendChild(paymentReference);
			
			Element externalCategory = message.createElement("v1:externalCategory");
			externalCategory.setTextContent(applicationProperties.getExternalCategory());
			requestBody.appendChild(externalCategory);
			
			Element externalChannel = message.createElement("v1:externalChannel");
			externalChannel.setTextContent(applicationProperties.getExternalChannel());
			requestBody.appendChild(externalChannel);
			
			Element webUser = message.createElement("v1:webUser");
			webUser.setTextContent(applicationProperties.getWebUser());
			requestBody.appendChild(webUser);
			
			Element webPassword = message.createElement("v1:webPassword");
			webPassword.setTextContent(applicationProperties.getWebPassword());
			requestBody.appendChild(webPassword);
			
			Element merchantName = message.createElement("v1:merchantName");
			merchantName.setTextContent(applicationProperties.getMerchantName());
			requestBody.appendChild(merchantName);
			
			Element itemName = message.createElement("v1:itemName");
			itemName.setTextContent(vItemName);
			requestBody.appendChild(itemName);
			
			Element amount = message.createElement("v1:amount");
			amount.setTextContent(vAmount);
			requestBody.appendChild(amount);
			
			Element minutesToExpire = message.createElement("v1:minutesToExpire");
			minutesToExpire.setTextContent(applicationProperties.getMinutesToExpire());
			requestBody.appendChild(minutesToExpire);
			
			Element notificationChannel = message.createElement("v1:notificationChannel");
			notificationChannel.setTextContent(applicationProperties.getNotificationChannel());
			requestBody.appendChild(notificationChannel);
			
			DOMSource domSource = new DOMSource(message);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			// transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
			// "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

			java.io.StringWriter sw = new java.io.StringWriter();
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
			requestXml = sw.toString();

			System.out.println(requestXml);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return requestXml;
	}
}
