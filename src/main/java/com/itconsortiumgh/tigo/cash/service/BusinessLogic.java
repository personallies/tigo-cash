package com.itconsortiumgh.tigo.cash.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.tigo.cash.model.IniPaymentLogs;
import com.itconsortiumgh.tigo.cash.model.IniPaymentRequest;
import com.itconsortiumgh.tigo.cash.model.IniPaymentResponse;
import com.itconsortiumgh.tigo.cash.model.PayBillRequest;
import com.itconsortiumgh.tigo.cash.model.TransStatus;
import com.itconsortiumgh.tigo.cash.properties.ApplicationProperties;
import com.itconsortiumgh.tigo.cash.repository.LocPaymentRepository;
import com.itconsortiumgh.tigo.cash.utils.SoapMessagePoster;
import com.itconsortiumgh.tigo.cash.utils.TransactionIdGenerator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BusinessLogic {
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	IniPayRequestBuilder iniPayRequestBuilder;
	@Autowired
	IniPayResponseBuilder iniPayResponseBuilder;
	@Autowired
	SoapMessagePoster soapMessagePoster;
	@Autowired
	LocPaymentRepository locPaymentRepository;
	@Autowired 
	PayBillRequestBuilder payBillRequestBuilder;
	
	public IniPaymentResponse initiatePayment(IniPaymentRequest iniPaymentRequest){
		IniPaymentResponse iniPaymentResponse = new IniPaymentResponse();
		IniPaymentLogs locPaymentLogs = new IniPaymentLogs();
		
		String vTransactionID = TransactionIdGenerator.nextId().toString();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date now = new Date();
		String myDate = dateFormat.format(now);
		String itcDate = "ITC"+myDate;
		String vCorrelationID = itcDate;
		String vPaymentReference = itcDate;
		
		String vItemName = iniPaymentRequest.getItemName();
		String vAmount = iniPaymentRequest.getAmount();
		String vMsisdn = iniPaymentRequest.getCustMsisdn();
		
		String requestXml = iniPayRequestBuilder.getXmlRequest(applicationProperties, vTransactionID, vCorrelationID, vMsisdn, vPaymentReference, vItemName, vAmount);
		log.info("Going to {} for a response",applicationProperties.getIniPayUrl());
		String responseXml = soapMessagePoster.sendMessage(applicationProperties.getIniPayUrl(), requestXml);
		log.info("The  Xml response :{}",responseXml);
		iniPaymentResponse = iniPayResponseBuilder.produceResponse(responseXml);
		log.info("The json response: {}",iniPaymentResponse);
		
		locPaymentLogs.setTransactionID(vTransactionID);
		locPaymentLogs.setCorrelationID(vCorrelationID);
		locPaymentLogs.setStatus(TransStatus.PENDING);
		locPaymentLogs.setCreated(new Date());
		locPaymentRepository.save(locPaymentLogs);
		
		return iniPaymentResponse;
	}
	
	public String payBill(PayBillRequest payBillRequest){
		
		String vMsisdn = payBillRequest.getMsisdn();
		String vAmount = payBillRequest.getAmount();
		String vTransactionId = TransactionIdGenerator.nextId().toString();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date now = new Date();
		String myDate = dateFormat.format(now);
		String itcDate = "ITC"+myDate;
		String vCorrelationId = itcDate;
		
		String requestXml = payBillRequestBuilder.getXmlRequest(applicationProperties, vTransactionId, vCorrelationId, vMsisdn, vAmount);
		log.info("Going to {} for a response",applicationProperties.getPayBillUrl());
		String responseXml = soapMessagePoster.sendMessage(applicationProperties.getPayBillUrl(), requestXml);
		log.info("The Xml Response :{}", responseXml);
		
		return responseXml;
		
	}
}
